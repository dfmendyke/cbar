// Time-stamp: <2023-04-23 09:00:08 daniel>

/*
  Copyright (c) 2023, Archadious
  This software is provided "as is", without warranty of any kind,
  express or implied, including but not limited to the warranties
  of merchantability, fitness for a particular purpose and
  noninfringement, in no event shall the authors or copyright
  holders be liable for any claim, damages or other liability,
  whether in an action of contract, tort or otherwise, arising
  from, out of or in connection with the software or the use
  or other dealings in the software.
*/


// Headers
//-----------------------------------------------------------------------------
#include <libopencm3/stm32/rcc.h>
#include "rcc.hh"


//-----------------------------------------------------------------------------
g0x::rcc::rcc() {

  rcc_clock_setup( &rcc_clock_config[ RCC_CLOCK_CONFIG_HSI_PLL_64MHZ ] );

  rcc_periph_clock_enable( RCC_GPIOA );
  rcc_periph_clock_enable( RCC_GPIOB );
  rcc_periph_clock_enable( RCC_GPIOC );

};  // end constructor
