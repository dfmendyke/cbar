// Time-stamp: <2023-04-23 11:18:44 daniel>

#ifndef __APPLICATION_HH__
#define __APPLICATION_HH__


//-----------------------------------------------------------------------------
#include "rcc.hh"  // g0x::rcc
#include "systick.hh"  // g0x::systick
#include "bar.hh" // g0x::bar


//-----------------------------------------------------------------------------
namespace arm {


  //---------------------------------------------------------------------------
  class application {

  public:

    application( );  // constructor
    ~application( ) {};  //

    int main( );

    void display( );

  protected:

  private:

    g0x::rcc rcc;
    g0x::systick tick;
    twelve::bar bar;

  };  // end class application


};  // end NS arm


#endif  // __APPLICATION_HH__
