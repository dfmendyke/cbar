// Time-stamp: <2023-04-10 19:12:29 daniel>

/*
  Copyright (c) 2023, Archadious
  This software is provided "as is", without warranty of any kind,
  express or implied, including but not limited to the warranties
  of merchantability, fitness for a particular purpose and
  noninfringement, in no event shall the authors or copyright
  holders be liable for any claim, damages or other liability,
  whether in an action of contract, tort or otherwise, arising
  from, out of or in connection with the software or the use
  or other dealings in the software.
*/


#ifndef __RCC_HH__
#define __RCC_HH__


//-----------------------------------------------------------------------------
namespace g0x {


  //---------------------------------------------------------------------------
  class rcc {

  public:

    rcc();
    ~rcc() {};

  protected:

  private:

  };  // end class rcc


};  // end NS g0x


#endif  // __RCC_HH__
