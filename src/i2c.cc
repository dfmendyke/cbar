// Time-stamp: <2023-04-10 19:11:41 daniel>

/*
  Copyright (c) 2023, Archadious
  This software is provided "as is", without warranty of any kind,
  express or implied, including but not limited to the warranties
  of merchantability, fitness for a particular purpose and
  noninfringement, in no event shall the authors or copyright
  holders be liable for any claim, damages or other liability,
  whether in an action of contract, tort or otherwise, arising
  from, out of or in connection with the software or the use
  or other dealings in the software.
*/


// Header Files
//-----------------------------------------------------------------------------
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/i2c.h>
#include <libopencm3/stm32/gpio.h>
#include "i2c.hh"  // bus::i2c::i2c


//-----------------------------------------------------------------------------
g0x::i2c::i2c( addr_t addr ) : handle{ I2C2 }, addr( addr ) {

  // Set the clocks
  rcc_periph_clock_enable( RCC_I2C2 );
  rcc_periph_clock_enable( RCC_GPIOA );

  // Set alternate functions
  gpio_set_af( GPIOA, GPIO_AF6, GPIO11 | GPIO12 );
  gpio_mode_setup( GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO11 | GPIO12 );
  gpio_set_output_options( GPIOA, GPIO_OTYPE_OD, GPIO_OSPEED_50MHZ, GPIO11 | GPIO12 );

  // Disable the I2C before changing any configuration
  i2c_peripheral_disable( handle );
  i2c_set_speed( handle, i2c_speed_fm_400k, 64 );

  // if everything is OK enable bus
  i2c_peripheral_enable( handle );

};  // end constructor


//-----------------------------------------------------------------------------
g0x::i2c::~i2c() { };  // end destructor


//-----------------------------------------------------------------------------
void g0x::i2c::clear_nack(  ) {

  I2C_ICR( handle ) = I2C_ICR( handle ) | I2C_ICR_NACKCF;

};  // end i2c_clear_nack



//-----------------------------------------------------------------------------
bool g0x::i2c::stop_detected(  ) {

	return ( I2C_ISR( handle ) & I2C_ISR_STOPF );

};  // end i2c_stop_detected


/**
 * Run a write/read transaction to a given 7bit i2c address
 * If both write & read are provided, the read will use repeated start.
 * Both write and read are optional
 * @param i2c peripheral of choice, eg I2C1
 * @param addr 7 bit i2c device address
 * @param w buffer of data to write
 * @param wn length of w
 * @param r destination buffer to read into
 * @param rn number of bytes to read (r should be at least this long)
 */
//-----------------------------------------------------------------------------
void g0x::i2c::out( const uint8_t *w, size_t wn ) {

	/*  waiting for busy is unnecessary. read the RM */
	if ( wn ) {
		i2c_set_7bit_address( handle, addr );
		i2c_set_write_transfer_dir( handle );
		i2c_set_bytes_to_transfer( handle, wn );
		i2c_enable_autoend( handle );
		i2c_send_start( handle );

		while ( wn-- ) {
			bool wait = true;
			while ( wait ) {
				if ( i2c_transmit_int_status( handle ) ) {
					wait = false;
				}
        //				while (i2c_nack(i2c)); /* FIXME Some error */
        if ( stop_detected() ) i2c_clear_stop( handle );
        if ( i2c_nack( handle ) ) {
          clear_nack();
          i2c_send_stop( handle );
        };  // end if i2c_nack
			};  // end while
			i2c_send_data( handle, *w++ );
		};  // end while
	};  // end if wn

};  // end out


#define ONE_BYTE 1
//-----------------------------------------------------------------------------
void g0x::i2c::write( const uint8_t byte ) {

  out( &byte, ONE_BYTE );

};  // end write byte


#define TWO_BYTES 2
//-----------------------------------------------------------------------------
void g0x::i2c::write( const uint8_t one, const uint8_t two ) {

  uint8_t bytes[] = { one, two };
  //  uint16_t bytes = static_cast< uint16_t >( ( one << 8 & 0x00ff ) ) + two;
  //out( reinterpret_cast< uint8_t* >( &bytes ), TWO_BYTES );
  out( bytes, TWO_BYTES );

};  // end write word
