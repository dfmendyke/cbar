// Time-stamp: <2023-04-24 15:29:04 daniel>

#ifndef __RAND_HH__
#define __RAND_HH__



//-----------------------------------------------------------------------------
namespace g041 {


  //---------------------------------------------------------------------------
  class rand {

  public:

    rand();  // constructor



  protected:

  private:

  };  // end class rand


};  // end NS g041


//-----------------------------------------------------------------------------
namespace arm {


  //---------------------------------------------------------------------------
  class dice : public g041::rand {

  public:

    dice();

  protected:

  private:

  };  // end class dice


};  // end NS arm


#endif  // __RAND_HH__
