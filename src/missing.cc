// Time-stamp: <2023-04-10 19:10:08 daniel>

/*
  Copyright (c) 2023, Archadious
  This software is provided "as is", without warranty of any kind,
  express or implied, including but not limited to the warranties
  of merchantability, fitness for a particular purpose and
  noninfringement, in no event shall the authors or copyright
  holders be liable for any claim, damages or other liability,
  whether in an action of contract, tort or otherwise, arising
  from, out of or in connection with the software or the use
  or other dealings in the software.
*/

// Define "missing" functions
//-----------------------------------------------------------------------------
extern "C" {

  void _kill( int ) { while ( 1 ); };
  void _getpid( ) { while ( 1 ); };

  extern void *__dso_handle __attribute__((__visibility__ ("hidden")));
  void *__dso_handle;

};  // end extern "C"
