// Time-stamp: <2023-04-10 19:13:09 daniel>

/*
  Copyright (c) 2023, Archadious
  This software is provided "as is", without warranty of any kind,
  express or implied, including but not limited to the warranties
  of merchantability, fitness for a particular purpose and
  noninfringement, in no event shall the authors or copyright
  holders be liable for any claim, damages or other liability,
  whether in an action of contract, tort or otherwise, arising
  from, out of or in connection with the software or the use
  or other dealings in the software.
*/


#ifndef __SYSTICK_HH__
#define __SYSTICK_HH__

#include <libopencm3/cm3/systick.h>  // System tick support


//-----------------------------------------------------------------------------
namespace g0x {

  using millisec_t = uint32_t;

  //---------------------------------------------------------------------------
  class systick {

  public:

    systick();  // constructor

    static void delay_ms( millisec_t );

  private:

  protected:

  };  // end systick

};  // end NS g0x


#endif  // __SYSTICK_HH__
