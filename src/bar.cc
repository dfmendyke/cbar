// Time-stamp: <2023-04-23 11:22:50 daniel>


#define ALL_BARS \
  GPIO12 | GPIO11 | GPIO8 | GPIO7 | GPIO6 | GPIO5 | GPIO4 | GPIO3 | GPIO2 | GPIO1 | GPIO0


//-----------------------------------------------------------------------------
#include <libopencm3/stm32/gpio.h>  // gpio_set / gpio_clear
#include "bar.hh"  // twlv::cbar


//-----------------------------------------------------------------------------
twelve::bar::bar( ) :
  port{ GPIOB, GPIOB, GPIOA, GPIOA, GPIOA, GPIOA, GPIOA, GPIOA, GPIOA, GPIOA, GPIOA, GPIOA, GPIOA },
  gpio{ GPIO3, GPIO3, GPIO12, GPIO11, GPIO8, GPIO7, GPIO4, GPIO5, GPIO6, GPIO3, GPIO2, GPIO1, GPIO0 }
{

  gpio_mode_setup( GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, ALL_BARS );
  gpio_mode_setup( GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO3 );

};  // end constructor


//-----------------------------------------------------------------------------
void twelve::bar::clear( ) {

  gpio_clear( GPIOA, ALL_BARS );
  gpio_clear( GPIOB, GPIO3 );

};  // end clear


//-----------------------------------------------------------------------------
void twelve::bar::all( ) {

  gpio_set( GPIOA, ALL_BARS );
  gpio_set( GPIOB, GPIO3 );

};  // end all


//-----------------------------------------------------------------------------
void twelve::bar::on( uint8_t index ) {

  if ( index < 13 ) gpio_set( port[ index ], gpio[ index ] );

};  // end on


//-----------------------------------------------------------------------------
void twelve::bar::off( uint8_t index ) {

  if ( index < 13 ) gpio_clear( port[ index ], gpio[ index ] );

};  // end off


//-----------------------------------------------------------------------------
void twelve::bar::alternate( ) {

  __asm__( "nop" );

};  // end alternate


//-----------------------------------------------------------------------------
void twelve::bar::set( uint8_t level ) {

  if ( level > 12 ) return;

  clear( );
  for ( size_t iter = 1; iter <= level; ++iter )
    gpio_set( port[ iter ], gpio[ iter ] );

};  // end set
