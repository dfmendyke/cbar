// Time-stamp: <2023-04-23 10:31:24 daniel>

#ifndef __BAR_HH__
#define __BAR_HH__


#include <array>  // std::array
#include <functional>  // std::function
#include <cstdint>

#define NUM_OF_BARS 12

//-----------------------------------------------------------------------------
namespace twelve {

  class bar {

  public:

    bar();  // constructor

    void clear( );  // all bars off
    void all( );  // all bars on
    void on( uint8_t );
    void off( uint8_t );

    void set( uint8_t );

    void alternate( );

  protected:

  private:


    std::array< int, NUM_OF_BARS + 1 > port;
    std::array< int, NUM_OF_BARS + 1 > gpio;

  };  // end class bar

};  // end NS twlv

#endif  // __BAR_HH__
