# Time-stamp: <2023-04-23 10:18:37 daniel>

##
# Module: src

src += \
  ./src/main \
  ./src/application \
  ./src/missing \
  ./src/rcc \
  ./src/systick \
  ./src/bar
