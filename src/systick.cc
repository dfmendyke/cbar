// Time-stamp: <2023-04-10 19:12:54 daniel>

/*
  Copyright (c) 2023, Archadious
  This software is provided "as is", without warranty of any kind,
  express or implied, including but not limited to the warranties
  of merchantability, fitness for a particular purpose and
  noninfringement, in no event shall the authors or copyright
  holders be liable for any claim, damages or other liability,
  whether in an action of contract, tort or otherwise, arising
  from, out of or in connection with the software or the use
  or other dealings in the software.
*/


// Header
//-----------------------------------------------------------------------------
#include "systick.hh"


//-----------------------------------------------------------------------------
static uint32_t system_millisec = 0;


extern "C" {
  //---------------------------------------------------------------------------
  void sys_tick_handler( void ) {

    ++system_millisec;

  };  // end sys_tick_handler

};  // extern "C"


//-----------------------------------------------------------------------------
g0x::systick::systick( ) {

  systick_set_reload( 64000 - 1 );
  systick_set_clocksource( STK_CSR_CLKSOURCE_AHB );
  systick_counter_enable();
  systick_interrupt_enable();

};  // end constructor


//-----------------------------------------------------------------------------
void g0x::systick::delay_ms( millisec_t millisec ) {

  millisec += system_millisec;
  while ( system_millisec <= millisec ) __asm__( "nop" );

};  // end delay_ms
