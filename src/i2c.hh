// Time-stamp: <2023-04-10 19:11:56 daniel>

/*
  Copyright (c) 2023, Archadious
  This software is provided "as is", without warranty of any kind,
  express or implied, including but not limited to the warranties
  of merchantability, fitness for a particular purpose and
  noninfringement, in no event shall the authors or copyright
  holders be liable for any claim, damages or other liability,
  whether in an action of contract, tort or otherwise, arising
  from, out of or in connection with the software or the use
  or other dealings in the software.
*/


#ifndef __I2C_HH__
#define __I2C_HH__

// Headers
//-----------------------------------------------------------------------------
#include <cstdint>  // uint8_t
#include <cstddef>  // size_t

//-----------------------------------------------------------------------------
namespace g0x {


  //---------------------------------------------------------------------------
  class i2c {

  public:

    using addr_t = uint8_t;

    i2c( addr_t );
    ~i2c( );

    void write( const uint8_t );
    void write( const uint8_t, const uint8_t );

  protected:

    void clear_nack( );
    bool stop_detected( );
    void out( const uint8_t*, size_t wn );

  private:

    uint32_t handle;
    uint8_t addr;

  };  // end i2c


};  // end g0x


#endif  // __I2C_HH__
