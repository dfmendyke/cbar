// Time-stamp: <2023-04-23 10:20:30 daniel>


//-----------------------------------------------------------------------------
#include "application.hh"  // arm::application


//-----------------------------------------------------------------------------
int main( void ) {

  arm::application application;
  return application.main();

};  // end main
