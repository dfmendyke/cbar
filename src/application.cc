// Time-stamp: <2023-06-09 21:58:17 daniel>

//-----------------------------------------------------------------------------
#include <libopencm3/stm32/gpio.h>
#include "application.hh"  // arm::application

#include <cstdlib>

//-----------------------------------------------------------------------------
arm::application::application( )
  : rcc(), tick(), bar() {

};  // end constructor



//-----------------------------------------------------------------------------
uint8_t number( ) {

  int r = ( rand() % 6 ) + ( rand() % 6 ) + 2;
  return r;

};  // end number



void arm::application::display( ) {

  int r1 = ( rand() % 6 ) + 1;
  int r2 = ( rand() % 6 ) + 1;
  r2 = 13 - r2;

  bar.clear( );
  for ( uint8_t iter = 1; iter < 13; ++iter ) {

    if ( ( iter <= r1 ) || ( iter >= r2 ) ) bar.on( iter );

  };  // end for


};  // end display


//-----------------------------------------------------------------------------
int arm::application::main( ) {

  gpio_mode_setup( GPIOC, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO15 );
  gpio_clear( GPIOC, GPIO15 );

  srand( 99 );

  while ( 1 ) {

    display( );
    tick.delay_ms( 275 );
    gpio_toggle( GPIOC, GPIO15 );
    tick.delay_ms( 275 );

  };  // end while

  return 0;  // should never reach this point

};  // end main
