# Time-stamp: <2023-04-22 23:11:13 daniel>


DEVICE = stm32g0
OPENCM3_DIR = ./cm3
name := cbar
src =

CFLAGS          += -Os -flto -ggdb3
CPPFLAGS        += -Os -MD --std=c++23 -ggdb3
LDFLAGS         += -static -specs=nano.specs -specs=nosys.specs -nostartfiles
LDLIBS          += -Wl,--start-group -lc -lgcc -lstdc++ -lnosys -Wl,--end-group

DEFS            += -DSTM32G0
LIBNAME          = opencm3_stm32g0


##
# Build rules for libopencm3
include $(OPENCM3_DIR)/mk/genlink-config.mk
include $(OPENCM3_DIR)/mk/gcc-config.mk

##
# Enumerate source files
include ./src/module.mk

##
# Define object and dependency
OBJS = $(addsuffix .o,$(src))
deps = $(addsuffix .d,$(src))
LDSCRIPT := $(name).ld
target := $(name).elf
hex := $(name).hex
bin := $(name).bin
LD := $(PREFIX)-g++


##
# Primary build target
all : $(hex) $(target)

##
# Commands for use with the Black Magic Probe
flash : $(target) ; $(PREFIX)-gdb $(target) --command=./mk/flash.cmd
debug : $(target) ; $(PREFIX)-gdb $(target) --command=./mk/debug.cmd

##
# Target to debug this Makefile
.PHONY : dump
dump :
	@echo OBJS: $(OBJS)
	@echo target: $(target) :: hex: $(hex)
	@echo PREFIX: $(PREFIX)
	@echo OBJCOPY: $(OBJCOPY)
	@echo LDSCRIPT: $(LDSCRIPT)
	@echo LIBDEPS: $(LIBDEPS)
	@echo Q: $(Q)
	@echo OBJDUMP: $(OBJDUMP)
	@echo LD: $(LD)
	@echo OPENCM3_DIR: $(OPENCM3_DIR)

##
# Locate build rules for compiling C++ source files
include $(OPENCM3_DIR)/mk/genlink-rules.mk
include $(OPENCM3_DIR)/mk/gcc-rules.mk
include ./mk/gpp-rules.mk


##
# Primary target
#$(target) : $(OBJS)
#	@printf "\nLinking main target: $@\n"
#	$(LD) $(OBJS) $(LDLIBS) $(LDFLAGS) -T$(LDSCRIPT) $(ARCH_FLAGS) -o $@


##
# HEX file from target
#$(hex) : $(target)
#	@printf "\nBuilding HEX file: $@\n"
#	$(OBJCOPY) -Oihex $< $@


##
# BIN file from target
#$(bin) : $(target)
#	@printf "\nBuilding BIN file: $@\n"
#	$(OBJCOPY) -Obinary $< $@


##
# Remove all objects created by this Makefile
.PHONY : clean
clean : ; rm --force $(OBJS) $(deps) $(target) $(hex)
